import 'package:flutter/material.dart';
import 'package:pt_project_01/constants/constant.dart';
import 'package:pt_project_01/data/scoped_model/main.dart';
import 'package:pt_project_01/myfolders/pagesorg/homeOrg.dart';
import 'package:pt_project_01/views/pages/home.dart';
import 'package:pt_project_01/views/pages/log.dart';
import 'package:pt_project_01/views/pages/log_in.dart';
import 'package:pt_project_01/views/pages/sign_up.dart';
import 'package:scoped_model/scoped_model.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final MainModel _model = MainModel();
  bool _isAuthenticated = false;

  @override

  ///this is called first just after a constructor
  ///To be executed automaticall before build
  void initState() {
    
   _model.autoAuthenticate();
   ///Getting value from publishedsubject and assigning to isauthenticated
    _model.userSubject.listen((value) {
      setState(() {
        _isAuthenticated = value;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      child: MaterialApp(
        title: 'Team Up',
        debugShowCheckedModeBanner: false,
       // home: _isAuthenticated ? Home() : Log(),
        routes: {
          weild: (BuildContext context) => _isAuthenticated ? Home() : Log(),
          logPage: (BuildContext context) => Log(),
          logInPage: (BuildContext context) => LogIn(),
          signUnPage: (BuildContext context) => SignUp(),
          homePageOrg: (BuildContext contect) => HomeOrg(),
          home: (BuildContext context) => Home()
        },
      ),
      model: _model,
    );
  }
}
