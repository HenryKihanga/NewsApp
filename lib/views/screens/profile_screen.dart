import 'package:flutter/material.dart';
import 'package:pt_project_01/data/scoped_model/main.dart';
import 'package:scoped_model/scoped_model.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return  Container(
      height: 500,
      color: Colors.red,
      child: Center(
        child: RaisedButton(
          child: Text('Log Out'),
          onPressed: () {
            model.logOut();
          },
        ),
      ),
    );
      },
    );

   
  }
}
