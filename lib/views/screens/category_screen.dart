import 'package:flutter/material.dart';
import 'package:pt_project_01/models/albums.dart';
import 'package:pt_project_01/models/categories.dart';
import 'package:pt_project_01/views/components/Headers/simple_header.dart';
import 'package:pt_project_01/views/components/cards/category_card.dart';
import 'package:pt_project_01/views/components/cards/image_text_card.dart';

class CategoryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('build');
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            SizedBox(height: 50),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Text('Categories',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700)),
            ),
            SizedBox(
              height: 50,
            )
          ]),
        ),
        SliverGrid(
          delegate:
              SliverChildBuilderDelegate((BuildContext contect, int index) {
            return Padding(
              padding: EdgeInsets.only(
                  left: index.isEven ? 10 : 0, right: index.isOdd ? 10 : 0),
              child: CategoryCard(
                categories: categories[index],
              ),
            );
          }, childCount: categories.length),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              childAspectRatio: 1.4),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: 'RECENT POOST',
              day: 'Sports',
              padding: 10,
              showComponet: true,
              showIcon: false,
            ),
          ]),
        ),
        SliverGrid(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(
                    left: index.isEven ? 10 : 0, right: index.isOdd ? 10 : 0),
                child: ImageTextCard(
                  album: albums[index],
                  padding: 1,
                ),
              );
            }, childCount: albums.length),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisSpacing: 5, crossAxisSpacing: 5)),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              SimpleHeader(
                date: 'RECENT POOST',
                day: 'Lifestyle',
                padding: 10,
                showComponet: true,
                showIcon: false,
              ),
            ],
          ),
        ),
        SliverGrid(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(
                    left: index.isEven ? 10 : 0, right: index.isOdd ? 10 : 0),
                child: ImageTextCard(
                  album: albums[index],
                  padding: 1,
                ),
              );
            }, childCount: albums.length),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisSpacing: 5, crossAxisSpacing: 5)),
      ],
    );
  }
}





// import 'package:flutter/material.dart';
// import 'package:scoped_model/scoped_model.dart';
// import 'package:taarifa_app/data/scoped-model/main.dart';
// import 'package:taarifa_app/model/category.dart';
// import 'package:taarifa_app/model/news.dart';
// import 'package:taarifa_app/model/tag.dart';
// import 'package:taarifa_app/views/components/cards/image_side_text_card.dart';

// import 'package:taarifa_app/views/components/headers/simple_header.dart';
// import 'package:taarifa_app/views/components/labels/label_line.dart';
// import 'package:taarifa_app/views/components/pillets/text_pillet.dart';
// import 'package:taarifa_app/views/components/search/search_bar.dart';
// import 'package:taarifa_app/views/pages/news_detailed_page.dart';

// class SearchScreen extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ScopedModelDescendant(
//       builder: (BuildContext context, Widget child, MainModel model) {
//         return Stack(
//           children: <Widget>[
//             CustomScrollView(
//               slivers: <Widget>[
//                 SliverList(
//                   delegate: SliverChildListDelegate([
//                     SizedBox(
//                       height: 30,
//                     ),
//                     SimpleHeader(
//                       fontSize: 30,
//                       padding: 20,
//                       title: 'Search',
//                     ),
//                     SearchBar(
//                       hintText: 'Search',
//                       padding: 20,
//                       onSearchBar: (val) {
//                         print(val);
//                       },
//                       showFilter: false,
//                     ),
//                     SizedBox(
//                       height: 30,
//                     ),
//                     SimpleHeader(
//                       fontSize: 20,
//                       padding: 20,
//                       title: 'Popular Categories',
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                   ]),
//                 ),
//                 SliverGrid(
//                   delegate: SliverChildBuilderDelegate(
//                       (BuildContext context, int index) {
//                     return Padding(
//                       padding: EdgeInsets.only(
//                           left: index.isEven ? 20 : 0,
//                           right: index.isOdd ? 20 : 0),
//                       child: TextPillet(
//                         title: firstCategories[index].name,
//                         onPilletTap: () {
//                           model.toggleshowSearchCancel(status: true);
//                         },
//                       ),
//                     );
//                   }, childCount: firstCategories.length),
//                   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                       crossAxisCount: 2,
//                       childAspectRatio: 4.1,
//                       mainAxisSpacing: 5,
//                       crossAxisSpacing: 5),
//                 ),
//                 SliverList(
//                   delegate: SliverChildListDelegate([
//                     SizedBox(
//                       height: 30,
//                     ),
//                     SimpleHeader(
//                       fontSize: 20,
//                       padding: 20,
//                       title: 'Popular Tags',
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                   ]),
//                 ),
//                 SliverList(
//                   delegate: SliverChildBuilderDelegate(
//                       (BuildContext context, int index) {
//                     return Padding(
//                       padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
//                       child: LabelLine(
//                         textColor: Colors.lightBlue,
//                         title: tags[index].name,
//                         oLabelLineTap: () {
//                           model.toggleshowSearchCancel(status: true);
//                         },
//                       ),
//                     );
//                   }, childCount: tags.length),
//                 )
//               ],
//             ),
//             model.showSearchCancel
//                 ? Container(
//                     margin: EdgeInsets.only(top: 220),
//                     height: MediaQuery.of(context).size.height,
//                     color: Colors.black26,
//                     child: ListView.builder(
//                       itemBuilder: (BuildContext context, int index) {
//                         return ImageSideTextCard(
//                           news: popularNews[index],
//                           onTap: () {
//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => NewsDetailedPage(
//                                           news: popularNews[index],
//                                         )));
//                           },
//                         );
//                       },
//                       itemCount: popularNews.length,
//                     ),
//                   )
//                 : Container()
//           ],
//         );
//       },
//     );
//   }
// }
