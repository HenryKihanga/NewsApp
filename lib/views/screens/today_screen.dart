import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pt_project_01/models/albums.dart';
import 'package:pt_project_01/views/components/Headers/simple_header.dart';
import 'package:pt_project_01/views/components/cards/album_card.dart';
import 'package:pt_project_01/views/components/cards/detailed_news_card.dart';
import 'package:pt_project_01/views/components/cards/image_text_card.dart';
import 'package:pt_project_01/views/components/cards/last_album_card.dart';

class TodayScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      //scrollDirection: Axis.horizontal,
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: 'wed ,20,Aug ,2019',
              day: 'Today',
              padding: 10,
            )
          ]),
        ),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return index.isEven
                ? Albumcard(
                    album: albums[index],
                    padding: 10,
                  )
                : DetailedNewsCard(
                    album: albums[index],
                    padding: 10,
                  );
          }, childCount: albums.length),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: 'wed ,12,Aug ,2019',
              day: 'Monday',
              padding: 10,
            )
          ]),
        ),
        SliverGrid(
            delegate:
                SliverChildBuilderDelegate((BuildContext context, int index) {
              return Padding(
                padding: EdgeInsets.only(
                    left: index.isEven ? 10 : 0, right: index.isOdd ? 10 : 0),
                child: ImageTextCard(
                  album: albums[index],
                  padding: 1,
                ),
              );
            }, childCount: albums.length),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisSpacing: 5, crossAxisSpacing: 5)),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return LastAlbumCard(
              album: albums[index],
              padding: 10,
            );
          }, childCount: albums.length),
        ),
      ],
    );
  }
}
