import 'package:flutter/material.dart';
import 'package:pt_project_01/models/albums.dart';
import 'package:pt_project_01/views/components/Headers/simple_header.dart';
import 'package:pt_project_01/views/components/cards/detailed_news_card.dart';
import 'package:pt_project_01/views/components/cards/image_text_card.dart';
import 'package:pt_project_01/views/components/cards/last_album_card.dart';

class DiscoveriesScreen extends StatelessWidget {
  final Album album;

  const DiscoveriesScreen({Key key, this.album}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return CustomScrollView(
      slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: 'THIS WEEK',
              day: 'Discover',
              padding: 10,
              showComponet: true,
            ),
          ]),
        ),
        SliverToBoxAdapter(
          child: Container(
            height: height / 3,
            child: ListView.builder(
              itemCount: albums.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: width * 4 / 5,
                  child: ImageTextCard(
                    album: albums[index],
                    padding: 10,
                  ),
                );
              },
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: 'Popular',
              day: 'Author',
              padding: 10,
              showComponet: true,
              showIcon: false,
            ),
          ]),
        ),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage(albums[index].avatar),
                ),
                title: Text(albums[index].author),
                subtitle: Text(albums[index].subtitle),
                trailing: IconButton(
                  icon: Icon(
                    Icons.star,
                    color: Colors.yellow,
                  ),
                  onPressed: () {},
                ),
              ),
            );
          }, childCount: albums.length),
        ),

    
        SliverList(
            delegate: SliverChildListDelegate(
          [
            SimpleHeader(
              date: '',
              day: 'The fashion Week',
              padding: 10,
            ),
          ],
        )),
        SliverToBoxAdapter(
          child: Container(
            height: height / 2,
            child: ListView.builder(
              itemCount: albums.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: width * 4 / 5,
                  child: DetailedNewsCard(
                    album: albums[index],
                    padding: 10,
                  ),
                );
              },
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            SimpleHeader(
              date: '',
              day: 'In Lifestyle',
              padding: 10,
              showComponet: true,
              showIcon: false,
            ),
          ]),
        ),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return LastAlbumCard(
              album: albums[index],
              padding: 10,
            );
          }, childCount: albums.length),
        ),
        SliverList(
            delegate: SliverChildListDelegate(
          [
            SimpleHeader(
              date: '',
              day: 'Must see',
              padding: 10,
            ),
          ],
        )),
        SliverToBoxAdapter(
          child: Container(
            height: MediaQuery.of(context).size.height / 3,
            child: ListView.builder(
              itemCount: albums.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  width: MediaQuery.of(context).size.width * 4 / 5,
                  child: ImageTextCard(
                    album: albums[index],
                    padding: 10,
                  ),
                );
              },
            ),
          ),
        ),
        SliverList(
            delegate: SliverChildListDelegate(
          [
            SimpleHeader(
              date: '',
              day: 'Poular Last Week',
              padding: 10,
            ),
          ],
        )),
        SliverList(
          delegate:
              SliverChildBuilderDelegate((BuildContext context, int index) {
            return LastAlbumCard(
              showAuthor: false,
              album: albums[index],
              padding: 10,
            );
          }, childCount: albums.length),
        ),
      ],
    );
  }
}
