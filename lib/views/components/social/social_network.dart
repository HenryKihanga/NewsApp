import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:url_launcher/url_launcher.dart';

class SocialNetworks extends StatelessWidget {
  final String _urlfacebook;
   final String _urltwitter;
    final String _urlinster;
  //final String _text;

  SocialNetworks(this._urlfacebook,this._urltwitter,this._urlinster,);

  _launchURLfacebook() async {
    if (await canLaunch(_urlfacebook)) {
      await launch(_urlfacebook);
    } else {
      throw 'Could not launch $_urlfacebook';
    }
  }
    _launchURLtwitter() async {
    if (await canLaunch(_urltwitter)) {
      await launch(_urltwitter);
    } else {
      throw 'Could not launch $_urltwitter';
    }
  }
    _launchURLinster() async {
    if (await canLaunch(_urlinster)) {
      await launch(_urlinster);
    } else {
      throw 'Could not launch $_urlinster';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.centerRight,
            child: InkWell(
              onTap: _launchURLfacebook,
             
              child: Container(
                child: Icon(
                  FontAwesomeIcons.facebookF,
                  color: Colors.blue[800],
                ),
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[100],
                    border: Border.all(color: Colors.grey, width: 2)),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: _launchURLtwitter,
            
            child: Container(
              child: Icon(
                FontAwesomeIcons.twitter,
                color: Colors.blueAccent,
              ),
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.grey[100],
                  border: Border.all(color: Colors.grey, width: 2)),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Align(
            alignment: Alignment.centerLeft,
            child: InkWell(
              onTap: _launchURLinster,
            
              child: Container(
                child: Icon(
                  FontAwesomeIcons.instagram,
                  color: Colors.pink[800],
                ),
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey[100],
                    border: Border.all(color: Colors.grey, width: 2)),
              ),
            ),
          ),
        )
      ],
    );
  }
}
