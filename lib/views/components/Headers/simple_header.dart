import 'package:flutter/material.dart';

class SimpleHeader extends StatelessWidget {
  final double padding;
  final String date;
  final String day;
  final bool showComponet;
  final bool showIcon;

  const SimpleHeader(
      {Key key,
      @required this.date,
      @required this.day,
      @required this.padding,
      this.showComponet = false,
      this.showIcon = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.all(padding),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 50,
                ),
                Text(date),
                Text(
                  day,
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 30,
                )
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 10, top: 20),
              child: Align(alignment: Alignment.topRight,
                            child: showComponet
                    ? (showIcon
                        ? Icon(Icons.star, color: Colors.blue,size: 45,)
                        : RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                            color: Colors.blue,
                            
                            child: Text('See All'),
                            onPressed: () {
                              print('prints');
                            },
                          ))
                    : Container(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
