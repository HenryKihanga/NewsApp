import 'package:flutter/material.dart';
import 'package:pt_project_01/models/albums.dart';

class LastAlbumCard extends StatelessWidget {
  final double padding;
  final Album album;
  final bool showAuthor;

  const LastAlbumCard(
      {Key key, @required this.padding, @required this.album, this.showAuthor = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      height: height / 4,
      width: width,
      padding: EdgeInsets.all(padding),
      child: Row(
        //  mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              width: width * 2 / 5,
              height: height / 5,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: AssetImage(album.cover), fit: BoxFit.cover)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    album.newscategory,
                    style: TextStyle(color: Colors.grey),
                  ),
                  Text(album.details,
                      maxLines: 3,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                  SizedBox(
                    height: 45,
                  ),
                  showAuthor? Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: AssetImage(album.avatar),
                        radius: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Text(
                          album.author,
                          style: TextStyle(color: Colors.grey),
                        ),
                      )
                    ],
                  ):Row(
                    children: <Widget>[
                    Icon(Icons.access_time),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Text(
                          album.lastseen,
                          style: TextStyle(color: Colors.grey),
                        ),
                      )
                    ],
                  ),
                  Divider(
                    color: Colors.black,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
