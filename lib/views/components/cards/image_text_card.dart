import 'package:flutter/material.dart';
import 'package:pt_project_01/models/albums.dart';

class ImageTextCard extends StatelessWidget {
  final Album album;
   final double padding;

  const ImageTextCard({Key key, @required this.album, this.padding}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    
    return Container(
   padding: EdgeInsets.all(padding),
      height: height / 3,
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), ),
      child: Stack(
        children: <Widget>[
          Container(
            height: height / 3,
            width: width,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: DecorationImage(
                    image: AssetImage(album.cover), fit: BoxFit.cover)),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                width: width,
                height: height / 12,
                decoration: BoxDecoration(
                  color: Colors.black26,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15)),
                ),
                padding: EdgeInsets.all(5),
                child: Text(
                  album.author + " Anounces His new Album",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                )),
          )
        ],
      ),
    );
  }
}
