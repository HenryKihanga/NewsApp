import 'package:flutter/material.dart';
import 'package:pt_project_01/models/categories.dart';


class CategoryCard extends StatelessWidget {
final Category categories;

  const CategoryCard({Key key, @required this.categories}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: categories.color, borderRadius: BorderRadius.circular(15)),
      height: MediaQuery.of(context).size.height / 6,
      width: MediaQuery.of(context).size.width / 2,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 10, right: 10),
            child: Align(
              alignment: Alignment.topRight,
              child: Image(
                image: AssetImage(categories.icon),
                height: 60,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, bottom: 10),
            child: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  categories.name,
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    );
  }
}
