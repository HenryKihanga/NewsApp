import 'package:flutter/material.dart';

import 'package:pt_project_01/constants/constant.dart';
import 'package:pt_project_01/views/components/social/social_network.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _passwordKey = GlobalKey<FormFieldState>();
  final _confirmPasswordKey = GlobalKey<FormFieldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.purpleAccent,
          ),
          onPressed: () {
            Navigator.pushNamed(context, logPage);
          },
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Form(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 5,
              ),
              Text(
                'TeamUp',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              Text(
                'Please fill in all details',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w100),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 40, right: 30, bottom: 5),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30)),
                      hintText: '*****@example.com',
                      labelText: 'Email address'),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 5, right: 30, bottom: 5),
                child: TextFormField(
                  key: _passwordKey,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30)),
                      hintText: '*************',
                      labelText: 'Password'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 5, right: 30, bottom: 10),
                child: TextFormField(
                  key: _confirmPasswordKey,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      hintText: "*********",
                      labelText: "Confirm Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30))),
                  validator: (value) {
                    if (value != _passwordKey.currentState.value) {
                      return 'Password do not match';
                    } else
                      return null;
                  },
                ),
              ),
              SizedBox(
                height: 50,
              ),

              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 30, top: 10, right: 30, bottom: 30),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.purpleAccent,
                        child: Text(
                          'SIGN UP',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          if (_confirmPasswordKey.currentState.validate()) {
                            {
                              Navigator.pushReplacementNamed(context, home);
                            }
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 70,
              ),

              //conecting to social network button
              SocialNetworks(
                  'https://www.facebook.com/profile.php?id=100006165432122',
                  'https://twitter.com/login',
                  'https://www.instagram.com/?hl=en'),
            ],
          ),
        ),
      ),
    );
  }
}
