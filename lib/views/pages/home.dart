import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:pt_project_01/views/screens/category_screen.dart';
import 'package:pt_project_01/views/screens/descoveries_screen.dart';
import 'package:pt_project_01/views/screens/profile_screen.dart';
import 'package:pt_project_01/views/screens/search_screen.dart';
import 'package:pt_project_01/views/screens/today_screen.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    List<Widget> _screans = <Widget>[
      TodayScreen(),

      CategoryScreen(),
      DiscoveriesScreen(),
      SearchScreen(),
      ProfileScreen(),
    ];
    return Scaffold(
        body: DefaultTabController(
          length: 5,
          child: _screans.elementAt(_currentIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          onTap: _onTabTapped,
          currentIndex: _currentIndex,
          showUnselectedLabels: true,
          unselectedItemColor: Colors.black,
          selectedItemColor: Colors.red,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.file,
              ),
              title: Text(
                "Today",
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.layerGroup,
              ),
              title: Text(
                "Categories",
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.star,
              ),
              title: Text(
                "Discover",
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                FontAwesomeIcons.search,
              ),
              title: Text(
                "Search",
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
              Icons.person,
              ),
              title: Text(
                "Profile",
              ),
            )
          ],
        ));
  }

  void _onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
