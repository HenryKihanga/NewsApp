import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pt_project_01/constants/constant.dart';
import 'package:pt_project_01/views/components/social/social_network.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  //focus nodes
  FocusNode _emailFocusNode = FocusNode();
  FocusNode _passwordFocusNode = FocusNode();

  // text editer controler for capturing inputs
  TextEditingController _emailTexteditingcontroller = TextEditingController();
  TextEditingController _passwordTexteditingcontroller =
      TextEditingController();

  //form key used to control form states
  final _signinFormkey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _isObscure = true;

  //Temporary login
  String _emailAddress = "teamup@gmail.com";
  String _passWord = "admin";

  @override
  Widget build(BuildContext context) {
    print("Build is called");
    return Scaffold(
      key: _scaffoldKey,
      //These are appBar Configurations
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.deepPurple,
          ),
          onPressed: () {
            Navigator.pushNamed(context, logPage);
          },
        ),
        actions: <Widget>[],
      ),

      // this is main body
      body: SingleChildScrollView(
        child: Form(
          key: _signinFormkey,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 55,
              ),
              Text(
                'TeamUp',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              Text(
                'Please fill in all details',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w100),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 40, right: 30, bottom: 5),
                child: TextFormField(
                  focusNode: _emailFocusNode,
                  controller: _emailTexteditingcontroller,
                  validator: (value) {
                    if (value.isEmpty)
                      return "Email is Required";
                    else
                      return null;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30)),
                      hintText: '*****@example.com',
                      labelText: 'Email address'),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, top: 5, right: 30, bottom: 1),
                child: TextFormField(
                  obscureText: _isObscure,
                  focusNode: _passwordFocusNode,
                  controller: _passwordTexteditingcontroller,
                  validator: (value) {
                    if (value.isEmpty)
                      return "Password is Required";
                    else
                      return null;
                  },
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(_isObscure
                            ? FontAwesomeIcons.eye
                            : FontAwesomeIcons.eyeSlash),
                        onPressed: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });

                          print(_isObscure);
                        },
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30)),
                      hintText: '*************',
                      labelText: 'Password'),
                ),
              ),
              FlatButton(
                child: Text('Forgot your password?',
                    style: TextStyle(
                        color: Colors.deepPurple, fontWeight: FontWeight.bold)),
                onPressed: () {},
              ),
              SizedBox(
                height: 55,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30, right: 30),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        color: Colors.deepPurple,
                        child: Text(
                          'LOG IN',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          //any code of login button should be inside if syatement(validated)
                          if (_signinFormkey.currentState.validate()) {
                            if (_emailAddress ==
                                    _emailTexteditingcontroller.text &&
                                _passWord ==
                                    _passwordTexteditingcontroller.text) {
                              Navigator.pushReplacementNamed(context, home);
                            } else {
                              print("imegomafgcvjbnklj,m. hgvjbmnnbnm");
                              _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: ListTile(
                                  leading: Icon(Icons.error, color: Colors.red),
                                  title: Text('Incorrect Email or Password'),
                                  trailing:
                                      Icon(Icons.error, color: Colors.red),
                                ),
                                backgroundColor: Colors.red,
                                duration: Duration(seconds: 1),
                              ));
                            } // end of nested if
                          } //end of main if statement
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 90,
              ),
              //conecting to social network button
              SocialNetworks(
                  'https://www.facebook.com/profile.php?id=100006165432122',
                  'https://twitter.com/login',
                  'https://www.instagram.com/?hl=en'),
            ],
          ),
        ),
      ),
    );
  }
}
