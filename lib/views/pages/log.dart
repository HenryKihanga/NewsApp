import 'package:flutter/material.dart';
import 'package:pt_project_01/constants/constant.dart';

class Log extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 180,
            ),
            Center(
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.transparent,
                child: Image(
                  image: AssetImage('assets/icons/diamond.png'),
                  height: 90,
                ),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Text(
              'TeamUp',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            Text(
              'The app tagline goes here',
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w100),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.deepPurple,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FlatButton(
              child: Text(
                'LOG IN',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                Navigator.pushNamed(context, logInPage);
              },
            ),
            FlatButton(
              child: Text('SIGN UP', style: TextStyle(color: Colors.white)),
              onPressed: () {
                 Navigator.pushNamed(context, signUnPage);
              },
            )
          ],
        ),
      ),
    );
  }
}
