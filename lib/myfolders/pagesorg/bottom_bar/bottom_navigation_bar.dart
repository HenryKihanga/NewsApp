import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:pt_project_01/views/pages/place_hold_widget.dart';

class BottomNavigatioBarCaller extends StatelessWidget {
final Function addMethod;
final int currentIndex;

  BottomNavigatioBarCaller( this.addMethod , this.currentIndex);

 

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: onTabTapped,
      currentIndex: currentIndex,
      showUnselectedLabels: true,
      unselectedItemColor: Colors.black,
      selectedItemColor: Colors.red,
      items: [
        BottomNavigationBarItem(
          icon: Icon(
            FontAwesomeIcons.file,
          ),
          title: Text(
            "Today",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            FontAwesomeIcons.layerGroup,
          ),
          title: Text(
            "Categories",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            FontAwesomeIcons.star,
          ),
          title: Text(
            "Discover",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            FontAwesomeIcons.search,
          ),
          title: Text(
            "Search",
          ),
        ),
        BottomNavigationBarItem(
          icon: Icon(
            FontAwesomeIcons.pager,
          ),
          title: Text(
            "Pages",
          ),
        )
      ],
    );
  }

  void onTabTapped(int index) {
  addMethod(index);
  }
}
