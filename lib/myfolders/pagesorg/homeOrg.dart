import 'package:flutter/material.dart';

import 'package:pt_project_01/myfolders/pagesorg/bottom_bar/bottom_navigation_bar.dart';
import 'package:pt_project_01/myfolders/pagesorg/widgets/categories.dart';
import 'package:pt_project_01/myfolders/pagesorg/widgets/discover.dart';
import 'package:pt_project_01/myfolders/pagesorg/widgets/pages.dart';
import 'package:pt_project_01/myfolders/pagesorg/widgets/search.dart';
import 'package:pt_project_01/myfolders/pagesorg/widgets/today.dart';







class HomeOrg extends StatefulWidget {
  @override
  _HomeOrgState createState() => _HomeOrgState();
}

class _HomeOrgState extends State<HomeOrg> {
  int cIndex = 0;
  final List<Widget> _children = [
    Today(),
    Categories(),
    Discovers(),
    Search(),
    Pages()
  ];

  void _callMethod(int index) {
    setState(() {
      cIndex = index;
      print(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _children[cIndex],
        bottomNavigationBar: BottomNavigatioBarCaller(_callMethod , cIndex));
  }
}
