import 'package:flutter/material.dart';
import 'package:pt_project_01/models/albums.dart';
import 'package:pt_project_01/views/components/cards/album_card.dart';

class Today extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
          itemCount: albums.length,
          itemBuilder: (BuildContext context, int index) {
            return Albumcard(
              album: albums[index], padding: 10,
            ); //return AlbumCard class
          });
  }
}