import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pt_project_01/models/albums.dart';

class AlbumcardOrg extends StatelessWidget {
  final Album album;

  const AlbumcardOrg({Key key, @required this.album}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 40, right: 10, bottom: 5),
      child: Container(
        height: 500,
        child: Stack(
          children: <Widget>[
            Container(
              height: 500,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: DecorationImage(
                      image: AssetImage(album.cover),
                      fit: BoxFit.cover)),
            ),
            album.isHot
                ? Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Align(
                        alignment: Alignment.topRight,
                        child: RaisedButton.icon(
                          onPressed: () {},
                          color: Colors.pink,
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          label: Text(
                            "HOT",
                            style: TextStyle(color: Colors.white),
                          ),
                          icon:
                              Icon(FontAwesomeIcons.fire, color: Colors.white),
                        )),
                  )
                : Container(),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        album.tittle,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        album.subtitle,
                        style: TextStyle(
                          color: Colors.brown,
                          fontSize: 20,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage(album.avatar),
                                    fit: BoxFit.cover),
                                border: Border.all(color: Colors.white)),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left:10),
                              child: Text(
                                album.author,
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                                alignment: Alignment.bottomRight,
                                child: Icon(
                                  Icons.comment,
                                  color: Colors.white,
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                              album.comments.toString(),
                              style: TextStyle(color: Colors.yellow),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
