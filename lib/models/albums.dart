import 'package:flutter/foundation.dart';

class Album {
  final String cover;
  final String tittle;
  final String subtitle;
  final String avatar;
  final String author;
  final int comments;
  final bool isHot;
  final String details;
  final String newscategory;
  final String lastseen;

  Album(
      {@required this.details,
       this.newscategory,
this.lastseen, 
      @required this.cover,
      @required this.tittle,
      @required this.subtitle,
      @required this.avatar,
      @required this.author,
      @required this.comments,
      @required this.isHot});
}

List<Album> albums = <Album>[
  Album(
      author: 'Henry Kihanga',
      avatar: 'assets/images/d11.jpeg',
      comments: 40,
      cover: 'assets/images/d0.jpeg',
      isHot: true,
      subtitle: '',
      tittle: '',
      lastseen: '4 hours ago',
      details: " I launch this as per the request of my funs",
      newscategory: 'EDUCATION',
       ),
      
  Album(
    author: 'William Kiluma',
    avatar: 'assets/images/d2.jpeg',
    comments: 20,
    cover: 'assets/images/d1.jpeg',
    isHot: true,
    lastseen: '6 hours ago',
    subtitle: 'Coding Experience',
    tittle: 'PT_2 @CoictUdsm',
    details: 'For the seek of good environment planting trees is innevitable ',
    newscategory: 'ENVIRONMENT'
  ),
  Album(
    author: 'lam Sheshe',
    avatar: 'assets/images/d4.jpeg',
    comments: 60,
    cover: 'assets/images/d3.jpeg',
    isHot: false,
    lastseen: 'Last Week',
    subtitle: 'Konde Boy',
    tittle: 'Mpya in town',
    details: ' I have three years experience on creating the background images',
    newscategory: 'CREATIVITY'
  ),
  Album(
    author: 'Kelvin Hongo',
    avatar: 'assets/images/d12.jpeg',
    comments: 60,
    cover: 'assets/images/d10.jpeg',
    isHot: false,
    lastseen: 'Last Month',
    subtitle: 'Senior system administrater',
    tittle: 'Currently working @Tara',
    details: 'This is the architectural design of the way heading to house welcome to be saved',
    newscategory: 'Design'
  )
];
