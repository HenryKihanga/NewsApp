import 'package:flutter/material.dart';

class Category {
  final String name;
  final String icon;
  final Color color;

  Category(
   {@required this.name,
   @required this.icon, 
   @required this.color});
}

List<Category> categories = <Category>[
  Category(
    name: 'Sports',
    icon: 'assets/images/categories/football.png',
    color: Colors.orange,
  ),
  Category(
    name: 'Travel',
    icon: 'assets/images/categories/travel.png',
    color: Colors.orange,
  ),
  Category(
    name: 'Music',
    icon: 'assets/images/categories/radio.png',
    color: Colors.cyan,
  ),
  Category(
    name: 'Gaming',
    icon: 'assets/images/categories/puzzle.png',
    color: Colors.pink[900],
  ),
  Category(
    name: 'Church',
    icon: 'assets/images/categories/church.png',
    color: Colors.blue,
  ),
  Category(
    name: 'Mosque',
    icon: 'assets/images/categories/mosque.png',
    color: Colors.green,
  ),
  Category(
    name: 'Photo',
    icon: 'assets/images/categories/camera.png',
    color: Colors.pink[300],
  ),
  Category(
    name: 'Food',
    icon: 'assets/images/categories/groceries.png',
    color: Colors.red,
  ),
];
