import 'package:flutter/material.dart';

class User {
  final String name;
  final String email;
  final String token;
  final int id;

  const User(
      {@required this.name,
      @required this.email,
      @required this.token,
      @required this.id});
  User.fromMap(Map<String, dynamic> map)
      : assert(map['id'] != null),
        assert(map['name'] != null),
        assert(map['email'] != null),
        assert(map['token'] != null),
        id = map['id'],
        name = map['name'],
        email = map['email'],
        token = map['token'];
}
