import 'package:flutter/foundation.dart';
import 'package:pt_project_01/models/user.dart';
import 'package:rxdart/subjects.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

mixin NewsConnectedModel on Model {
  /*mixin which will be used to put all of 
  the variables needed to be seen in every
   page of our application*/
}
mixin AlbumModel on NewsConnectedModel {}
mixin CategoryModel on NewsConnectedModel {}

mixin UtilityModel on NewsConnectedModel {
  bool _showSpinner = false;

  ///we are now creating getters
  bool get showSpinner => _showSpinner;
  void toggleSpinner() {
    _showSpinner = !showSpinner;
    notifyListeners();
  }
}
mixin UserModel on NewsConnectedModel {
  String _emailAddress = "a";
  String _passWord = "a";
  User _user;

  PublishSubject<bool> _userSubject = PublishSubject();

  Future<bool> logIn(
      {@required String email, @required String password}) async {
    bool _isLogedIn = false;
    final SharedPreferences pref = await SharedPreferences.getInstance();

    if (email == _emailAddress && password == _passWord) {
      _user = User(
          email: email, id: 1, name: 'HenryKihanga', token: "this is token");

      pref.setInt('id', _user.id);
      pref.setString('name', _user.name);
      pref.setString('email', _user.email);
      pref.setString('token', _user.token);
      _userSubject.add(true); //default setter
      _isLogedIn = true;
      notifyListeners();
    }
    return _isLogedIn;
  }

  void logOut() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
    _userSubject.add(false);
    notifyListeners();
  }

  Future<void> autoAuthenticate() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String _token = pref.getString("token");
    if (_token.isNotEmpty) {
      _user = User(
          email: pref.getString('email'),
          id: pref.getInt('id'),
          name: pref.getString('name'),
          token: pref.getString('token'));
          _userSubject.add(true);
          notifyListeners();
    }
    else{
         _userSubject.add(false);
          notifyListeners();
    }
  }

  ///getter
  PublishSubject get userSubject => _userSubject;
}
